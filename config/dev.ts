import type { UserConfigExport } from '@tarojs/cli';

export default {
  logger: {
    quiet: false,
    stats: true,
  },
  env: {
    NODE_ENV: '"development"', // JSON.stringify('development')
    // TARO_APP_ID: '"wxb19d3be970cdb4ba"',
  },
  mini: {},
  h5: {},
} satisfies UserConfigExport;
