module.exports = function (ctx) {
  ctx.register({
    name: "onPreviewComplete",
    fn: ({ success, data, error }) => {
      console.log("接收预览后数据", success, data, error);
      // 你可以在这里发送钉钉或者飞书消息
    },
  });
  ctx.register({
    name: "onUploadComplete",
    fn: ({ success, data, error }) => {
      console.log("接收上传后数据", success, data, error);
      // 你可以在这里发送钉钉或者飞书消息
    },
  });
};
