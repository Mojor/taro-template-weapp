## 相关技术文档

- [Taro](https://docs.taro.zone/docs/)
- [Taro UI](https://taro-ui.jd.com/#/docs/introduction)
- flooks
  - [React Hooks 状态管理器，可自动优化性能](https://github.com/nanxiaobei/flooks/blob/main/README.zh-CN.md)

## 开发准备

```sh
curl https://get.volta.sh | bash
volta install node@18.12.1
# 全局安装pnpm
npm i pnpm -g
# pnpm 安装依赖包
pnpm i
```

## 开发运行

```sh
# 本地开发
pnpm run dev:weapp
# 构建完后自动“上传代码作为体验版”（测试环境）
pnpm run build:weapp-test:upload
# 构建完后自动“上传代码作为正式版”（正式环境）
pnpm run build:weapp-prod:upload
# 构建正式环境“不上传到体验版”
pnpm run build:weapp-prod
```

## 项目规范

- 所有文件名皆为小写，多个单词用"-"连接，不允许出现驼峰命名文件
- 变量命名风格为驼峰
- 分包目录固定在 subpages 目录下
- 使用 flooks 做状态管理

## 注意事项

- hooks 文件内是公共代码逻辑，公共逻辑尽可能封装成自定义 hook 以达到代码复用的目的
- 不允许滥用`any`，公共的对象类型先再 models 目录定义类型
