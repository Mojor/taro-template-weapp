import Taro from '@tarojs/taro';

/**
 * 路径反斜杠替换处理
 * 例如：https:\/\/img5.chungoulife.com\/201912\/pic536c98a54551e2b01c1efe350bfdb6f4.jpg
 * 在微信浏览器会出现无法解析上面这种路径问题
 */
export const formatUrl = (val: string): string => val.replace(/\\/, '/');

/**
 * SDK版本号判断
 * @param v 需要判断的版本号
 * @returns
 */
export const SdkVersion = (v?: string) => {
  const version = Taro.getAppBaseInfo().SDKVersion ?? '';
  if (version < (v ?? '2.32.3')) {
    return true;
  } else {
    return false;
  }
};

/** 是否PC设备 */
export const isPC = ['mac', 'windows'].includes((0, Taro.getSystemInfoSync)().platform);
