import { View, Text } from '@tarojs/components';
import { useLoad } from '@tarojs/taro';
import { example } from '@/api/example';
import classnames from 'classnames';
import styles from './index.module.less';

export default function Index() {
  const getApi = async () => {
    example({}).then((res) => {
      console.log(res);
    });
  };
  useLoad(() => {
    console.log('Page loaded.');
    getApi();
  });

  return (
    <View className={classnames('index text-[#acc855] text-[100px]', styles.container)}>
      <Text>Hello world!</Text>
      <View className='line-clamp-2'>
        两行文字溢出省略号显示两行文字溢出省略号显示两行文字溢出省略号显示两行文字溢出省略号显示两行文字溢出省略号显示两行文字溢出省略号显示
      </View>
    </View>
  );
}
