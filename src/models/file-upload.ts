/* eslint-disable @typescript-eslint/no-namespace */
export namespace NSFileUpload {
  /*ICredentials*/
  export interface ICredentials {
    Token: string;
    TmpSecretId: string;
    TmpSecretKey: string;
  }

  /*IData*/
  export interface IData {
    ExpiredTime: number; // 密钥过期时间
    Expiration: string;
    Credentials: ICredentials;
    RequestId: string;
    StartTime: number;
    Cdn: string;
    Bucket: string;
    Region: string;
  }

  /*tsModel2*/
  export type IResult = API.ResponseType<IData>;
}
