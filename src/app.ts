import { PropsWithChildren } from 'react';
import { useLaunch } from '@tarojs/taro';
import './app.less';
import Update from './hooks/update';

function App({ children }: PropsWithChildren<any>) {
  useLaunch(() => {
    console.log('App launched.');
    Update();
  });

  // children 是将要会渲染的页面
  return children;
}

export default App;
