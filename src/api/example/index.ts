import request from '@/service/request';

const host = process.env.TARO_APP_API;

export const example = (data) => {
  return request({
    url: `${host}/v1/activity/index`,
    method: 'post',
    data,
    loading: true,
    useToken: false,
    openCodeErrTips: true,
    openCodeSuccessTips: false,
  });
};
