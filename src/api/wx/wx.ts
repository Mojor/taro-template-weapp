import request from '@/service/request';

const host = process.env.REACT_APP_BASE_API;

/**
 * 二维码
 * @param data
 * @returns
 */
export const wxQrcode = (data: WX_API.WXQrcode) => {
  return request({
    url: `${host}/v1/wxa/qrcode`,
    method: 'post',
    data,
    loading: true,
    useToken: true,
    openCodeErrTips: true,
  });
};

/**
 * 上传图片 获取临时秘钥
 * @param data
 * @returns
 */
export const getFileUploadToken = (data = {}) => {
  return request({
    url: `${host}/v1/common/get-cos-credential`,
    method: 'post',
    data,
    loading: true,
    useToken: true,
    openCodeErrTips: true,
  });
};
