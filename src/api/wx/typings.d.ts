/**
 * 微信 API
 */
declare namespace WX_API {
  /**
   * 微信二维码参数
   */
  interface WXQrcode {
    page: string; //页面路径
    scene: string; //参数
    env_version: string; //正式版为 "release"，体验版为 "trial"，开发版为 "develop"。默认是正式版。
  }
}
