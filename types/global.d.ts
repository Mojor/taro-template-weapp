/// <reference types="@tarojs/taro" />

declare module "*.png";
declare module "*.gif";
declare module "*.jpg";
declare module "*.jpeg";
declare module "*.svg";
declare module "*.css";
declare module "*.less";
declare module "*.scss";
declare module "*.sass";
declare module "*.styl";

declare namespace NodeJS {
  interface ProcessEnv {
    /** NODE 内置环境变量, 会影响到最终构建生成产物 */
    NODE_ENV: "development" | "production";
    /** 当前构建的平台 */
    TARO_ENV:
      | "weapp"
      | "swan"
      | "alipay"
      | "h5"
      | "rn"
      | "tt"
      | "quickapp"
      | "qq"
      | "jd";
    /**
     * 当前构建的小程序 appid
     * @description 若不同环境有不同的小程序，可通过在 env 文件中配置环境变量`TARO_APP_ID`来方便快速切换 appid， 而不必手动去修改 dist/project.config.json 文件
     * @see https://taro-docs.jd.com/docs/next/env-mode-config#特殊环境变量-taro_app_id
     */
    TARO_APP_ID: string;
  }
}

/**
 * 请求接口类型
 */
declare namespace API {
  /**
   * 请求响应类型
   */
  export interface ResponseType<T> {
    code: number;
    data: T;
    msg: string;
  }

  /**
   * 分页查询参数
   */
  export interface PaginationParams {
    page?: number;
    size?: number;
  }
  /**
   * 分页返回字段
   */
  export interface Pagination {
    /**
     * 是否下一页
     */
    more: boolean;
    /**
     * 页码
     */
    page: number;
    /**
     * 记录总数量
     */
    total: number;
    /**
     * 每页数量
     */
    page_num: number;
  }

  /**
   * 有分页的data返回数据类型
   */
  export interface PaginationType<T> {
    list: T[];
    /**
     * 是否下一页
     */
    more: boolean;
    /**
     * 页码
     */
    page: number;
    /**
     * 记录总数量
     */
    total: number;
    /**
     * 每页数量
     */
    page_num: number;
  }

  /**
   * 有分页返回类型<T 列表项数据>
   */
  export interface PaginationResponseType<T> {
    code: number;
    data: PaginationType<T>;
    msg: string;
  }
}
