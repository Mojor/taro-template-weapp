declare namespace REQUEST {
  export type Method =
    | 'get'
    | 'GET'
    | 'delete'
    | 'DELETE'
    | 'head'
    | 'HEAD'
    | 'options'
    | 'OPTIONS'
    | 'post'
    | 'POST'
    | 'put'
    | 'PUT'
    | 'patch'
    | 'PATCH'
    | 'purge'
    | 'PURGE'
    | 'link'
    | 'LINK'
    | 'unlink'
    | 'UNLINK';

  export type ResponseType = 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream';

  export type ResponseEncoding =
    | 'ascii'
    | 'ASCII'
    | 'ansi'
    | 'ANSI'
    | 'binary'
    | 'BINARY'
    | 'base64'
    | 'BASE64'
    | 'base64url'
    | 'BASE64URL'
    | 'hex'
    | 'HEX'
    | 'latin1'
    | 'LATIN1'
    | 'ucs-2'
    | 'UCS-2'
    | 'ucs2'
    | 'UCS2'
    | 'utf-8'
    | 'UTF-8'
    | 'utf8'
    | 'UTF8'
    | 'utf16le'
    | 'UTF16LE';

  export interface Options {
    url: string;
    method: Method;
    data?: any;
    loading?: boolean;
    loadingTitle?: string;
    contentType?: string;
    openErrTips?: boolean;
  }
  export interface RequestParams {
    /**
     * 请求路径
     */
    url: string;
    /**
     * 请求方法
     */
    method: Method;
    /**
     * 请求携带的数据
     */
    data?: any;
    /**
     * 请求头
     */
    header?: any;
    /**
     * 显示全局loading, 默认: false
     */
    loading?: boolean;
    /**
     * 是否显示透明蒙层，防止触摸穿透 默认: true
     */
    mask?: boolean;
    /**
     * loading时的标题 默认: 加载中...
     */
    loadingTitle?: string;
    /**
     * header 中的content-type,默认：application/json，也可以在header定义
     */
    contentType?: string;
    /**
     * 请求失败显示提示，默认: true
     */
    openErrTips?: boolean;
    /**
     * 自定义请求失败提示信息, 默认: 提示信息：网络不好，请求失败
     */
    errTipsContent?: string;
    /**
     * 请求头添加token， 默认: true
     */
    useToken?: boolean;
    /**
     * 业务请求失败显示提示(code不等于200)，默认: true
     */
    openCodeErrTips?: boolean;
    /**
     * 业务请求成功显示提示(code默认等于200)，默认: false
     */
    openCodeSuccessTips?: boolean;
    /**
     * 超时时间，单位为毫秒
     */
    timeout?: number;
    /**
     * 开启 http2
     */
    enableHttp2?: boolean;
    /**
     * 开启 quic
     */
    enableQuic?: boolean;
    /**
     * 开启 cache
     */
    enableCache?: boolean;
  }
}
